BASE=`dirname $0`

ENV=${ENV:-dev}

$BASE/run.sh loaddata --traceback initial_data
