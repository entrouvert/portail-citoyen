import json
from datetime import timedelta

from django.http import HttpResponse
from django.utils.timezone import now

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

from authentic2.views import EditProfile
from authentic2.saml.models import LibertyFederation, LibertyProvider

edit_profile = login_required(EditProfile.as_view())


def stats(request):
    User = get_user_model()
    last_day = now() - timedelta(days=1)
    last_hour = now() - timedelta(hours=1)
    qs = User.objects.all()
    what = {
            'users_count': qs.count(),
            'users_last_connection_less_than_one_day_ago': qs.filter(last_login__gte=last_day).count(),
            'user_last_connection_less_than_one_hour_ago': qs.filter(last_login__gte=last_hour).count(),
            'active_federations': LibertyFederation.objects.filter(sp__isnull=False).count(),
    }
    for sp in LibertyProvider.objects.filter(service_provider__isnull=False):
        what['%s_active_federations' % sp.slug] = LibertyFederation.objects.filter(sp=sp).count()
    return HttpResponse(json.dumps(what), mimetype='application/json')

def roles(request):
    what = [{'id': group.id, 'name': group.name} for group in Group.objects.all()]
    return HttpResponse(json.dumps(what), mimetype='application/json')
