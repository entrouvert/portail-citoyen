from django.db import models
from django.core.validators import RegexValidator, MinLengthValidator, \
    MaxLengthValidator
from django.utils.translation import ugettext_lazy as _, pgettext_lazy
from django.contrib.auth.models import Group


from authentic2.models import AbstractUser

from . import app_settings


class Citoyen(AbstractUser):
    title = models.CharField(
            verbose_name=unicode(pgettext_lazy('person title', 'title')),
            max_length=16,
            blank=True,
            choices=tuple((x, x) for x in (_('Mrs'),
                     _('Mr'))))

    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('e-mail address'), max_length=128, blank=True)
    city = models.CharField(_('city'), max_length=64, blank=True)
    address = models.CharField(_('address'), max_length=128, blank=True)
    postal_code = models.CharField(_('postal code'), max_length=5, blank=True,
            validators=[
                RegexValidator(app_settings.POSTAL_CODE_REGEXP,
                    app_settings.POSTAL_CODE_MESSAGE,
                    'invalid postal code')])
    phone = models.CharField(verbose_name=_("phone"),
            max_length=16, blank=True,
            help_text=_('Phone number must start with 01, 02, 03, 04, 05, 08 or 07 and be ten digits long without spaces'),
            validators=[
                RegexValidator(app_settings.HOME_PHONE_REGEXP,
                    app_settings.HOME_PHONE_MESSAGE,
                    'invalid phone'),])
    mobile = models.CharField(verbose_name=_("mobile"),
            max_length=16, blank=True,
            help_text=_('Mobile phone number must start with 06 or 07 and be ten digits long without spaces'),
            validators=[
                RegexValidator(app_settings.MOBILE_PHONE_REGEXP,
                    app_settings.MOBILE_PHONE_MESSAGE,
                    'invalid phone'),])

    REQUIRED_FIELDS = ['first_name', 'last_name', 'email']
    USERNAME = 'username'
    USER_PROFILE = ( 'title', 'username', 'first_name', 'last_name', 'email',
            'phone', 'mobile', 'address', 'postal_code', 'city', 'roles')
    REGISTER_FIELDS = ( 'title', 'username', 'first_name', 'last_name', 'email',
            'phone', 'mobile', 'address', 'postal_code', 'city')
from authentic2.attribute_aggregator.core import ATTRIBUTE_MAPPING

ATTRIBUTE_MAPPING['l']['profile_field_name'] = 'city'
ATTRIBUTE_MAPPING['personalTitle']['profile_field_name'] = 'title'
ATTRIBUTE_MAPPING['street']['profile_field_name'] = 'address'
ATTRIBUTE_MAPPING['postalCode']['profile_field_name'] = 'postal_code'
ATTRIBUTE_MAPPING['mobile']['profile_field_name'] = 'mobile'

class Role(Group):
    class Meta:
        proxy = True
        verbose_name = _('role')
        verbose_name_plural = _('roles')

import permission_hack
