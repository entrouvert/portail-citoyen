import urlparse
import urllib2
import time
import logging

from django.contrib import messages
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.core.exceptions import ValidationError

from django.contrib import admin
from django.contrib.auth.models import Group

from authentic2.saml.models import LibertyProvider, LibertyServiceProvider

from . import app_settings
from . import models

logger = logging.getLogger(__name__)

if settings.AUTH_USER_MODEL == 'portail_citoyen.Citoyen':
    import forms

    from django.contrib.auth.admin import UserAdmin
    class CitoyenAdmin(UserAdmin):
        list_display = UserAdmin.list_display + ('is_active',)
        non_superuser_fieldsets = (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name',
                'email', 'address', 'city', 'postal_code', 'phone',
                'mobile')}),
            (_('Permissions'), {'fields': ('is_active',
                'groups')}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )
        fieldsets = (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name',
                'email', 'address', 'city', 'postal_code', 'phone',
                'mobile')}),
            (_('Permissions'), {'fields': ('is_active',
                'is_superuser', 'groups')}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )
        form = forms.UserChangeForm
        add_form = forms.UserCreationForm
        add_fieldsets = (
                (None, {
                    'classes': ('wide',),
                    'fields': ('username', 'password1', 'password2', 'first_name', 'last_name', 'email')}
                ),
            )

        def get_fieldsets(self, request, obj=None):
            if not obj:
                return self.add_fieldsets
            if not request.user.is_superuser:
                return self.non_superuser_fieldsets
            return self.fieldsets


    if models.Citoyen in admin.site._registry:
        admin.site.unregister(models.Citoyen)
    admin.site.register(models.Citoyen, CitoyenAdmin)

from django.contrib.auth.admin import GroupAdmin

class GroupPortailCitoyenAdmin(GroupAdmin):
    fieldsets = ((None, {'fields': ('name', 'permissions')}),)
    default_fieldsets = ((None, {'fields': ('name', )}),)

    def get_fieldsets(self, request, obj=None):
        if request.user.is_superuser:
            return self.fieldsets
        else:
            return self.default_fieldsets

admin.site.register(models.Role, GroupPortailCitoyenAdmin)

from authentic2.compat import get_user_model
from django.db.models.signals import m2m_changed, pre_save
from django.dispatch import receiver

@receiver(pre_save, sender=get_user_model())
def update_is_staff_for_superuser(sender, instance, raw, using, update_fields,
        **kwargs):
    '''Super-users have always access to the admin site'''
    if raw:
        return
    if instance.is_superuser:
        instance.is_staff = True


if hasattr(get_user_model(), 'groups'):
    @receiver(m2m_changed, sender=get_user_model().groups.through)
    def update_is_staff(sender, instance, action, reverse, model, pk_set,
            using, **kwargs):
        if not reverse:
            if action.startswith('post_'):
                instance.is_staff = instance.groups \
                        .filter(permissions__isnull=False) \
                        .exists() or instance.is_superuser
                instance.save(update_fields=['is_staff'])
        else:
            for user in get_user_model().objects.filter(pk__in=pk_set):
                user.is_staff = user.groups \
                        .filter(permissions__isnull=False) \
                        .exists() or user.is_superuser
                user.save(update_fields=['is_staff'])


if 'wcsinst.wcsinst' in settings.INSTALLED_APPS:

    from wcsinst.wcsinst.admin import WcsInstanceAdmin
    from wcsinst.wcsinst.models import WcsInstance

    admin.site.unregister(WcsInstance)

    class WcsInstancePortailCitoyenAdmin(WcsInstanceAdmin):
        list_display = [ '__unicode__', 'link' ]
        fieldsets = (
            (None, {'fields': ('title', 'domain', 'link'),}),
            ('site-options.cfg',
                {'fields': ('postgresql', 'backoffice_feed_url' )}
            ),
            ('site-options.cfg au-quotidien',
                {'fields': ('drupal', 'ezldap', 'strongbox', 'clicrdv', 'domino' )}
            ),
        )
        readonly_fields = WcsInstanceAdmin.readonly_fields + ('link',)

        def link(self, obj):
            url = str(settings.WCSINST_URL_TEMPLATE % { 'domain': obj.domain })
            return format_html('<a class="external-link" href="{1}">{0}</a>',
                    url, url)
        link.allow_tags = True
        link.short_description = _('URL')

        def delete_model(self, request, obj):
            title = obj.title
            super(WcsInstancePortailCitoyenAdmin, self).delete_model(request, obj)
            Group.objects.filter(name__startswith=title+' - ').delete()

        def save_related(self, request, form, formsets, change):
            instance = form.instance
            admin.ModelAdmin.save_related(self, request, form, formsets, change)
            for key, value in app_settings.WCSINST_DEFAULT_VARIABLES.iteritems():
                if instance.variables.filter(key=key).count() == 0:
                    instance.variables.create(key=key, value=value)
            instance.notify()
            # Get and configure metadata
            sleep_length = 4
            c = 0
            done = False
            if LibertyProvider.objects.filter(slug=instance.domain).exists():
                provider = LibertyProvider.objects.get(slug=instance.domain)
                provider.name = instance.title
            else:
                provider = LibertyProvider(name=instance.title, slug=instance.domain)
            while not done:
                time.sleep(sleep_length)
                url = str(settings.WCSINST_URL_TEMPLATE % { 'domain': instance.domain })
                url = urlparse.urljoin(url, 'saml/metadata')
                try:
                    provider.metadata = urllib2.urlopen(url).read().decode('utf-8')
                except:
                    logger.warning("Unable to retrieve SAML 2.0 metadata for %r", instance.title, exc_info=True)
                    if c >= 3:
                        messages.error(request, _('Unable to retrieve SAML 2.0 metadatas, please report it to an administrator'))
                        break
                else:
                    try:
                        provider.clean()
                    except ValidationError, v:
                        logger.error('Unable to create the SAML 2.0 provider: %r', v)
                        if provider.id:
                            provider.delete()
                        messages.error(request, _('Unable to create the SAML 2.0 provider: %s') % v)
                        break
                    provider.save()
                    service_provider, created = LibertyServiceProvider \
                        .objects.get_or_create(
                                liberty_provider=provider,
                                enabled=True)
                    done = True
                c += 1
                sleep_length *= 2
                if done and 'auquotidien_plugin' in settings.INSTALLED_APPS:
                    from .apps.auquotidien_plugin.models import AuQuotidienAPI
                    defaults = {
                        'name': instance.title,
                        'orig': request.META['HTTP_HOST'],
                    }
                    defaults.update(app_settings.AUQUOTIDIENAPI_DEFAULTS)
                    auquotidien_api, created = AuQuotidienAPI.objects.get_or_create(
                            service_provider=service_provider,
                            defaults=defaults)

        def save_model(self, request, obj, form, change):
            if change:
                old_obj = WcsInstance.objects.get(pk=obj.pk)
            super(WcsInstancePortailCitoyenAdmin, self).save_model(request, obj, form, change)
            new_prefix = obj.title
            new_prefix += ' - '
            if change:
                # rename old admin group if needed
                old_prefix = old_obj.title + ' - '
                if old_prefix != new_prefix:
                    for group in Group.objects.select_for_update() \
                            .filter(name__startswith=old_prefix):
                        group.name = new_prefix + group.name[len(old_prefix):]
                        group.save()
            else:
                # create first group
                admin_group, created = Group.objects.get_or_create(name=new_prefix+'Administrateur')
                # make current user admin of the new site
                request.user.groups.add(admin_group)


    admin.site.register(WcsInstance,
            WcsInstancePortailCitoyenAdmin)



