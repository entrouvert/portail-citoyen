from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.generic.base import RedirectView

admin.autodiscover()

from authentic2.urls import not_homepage_patterns


from . import app_settings


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',
        name='logout', kwargs={'next_page': '/'}),
)

urlpatterns += patterns('',
    (r'^favicon\.ico$', RedirectView.as_view(url=app_settings.FAVICON_URL)),
    url(r'^', include(not_homepage_patterns)),
    url(r'^su/(?P<username>.*)/$', 'authentic2.views.su', {'redirect_url': '/'}),
    url(r'^stats/$', 'portail_citoyen.views.stats'),
    url(r'^roles/$', 'portail_citoyen.views.roles'),
)


urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )

if 'portail_citoyen.apps.msp' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
            url('^msp/', include('portail_citoyen.apps.msp.urls')),
    )

if 'cms_ajax_text_plugin' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
            url('^plugin/', include('cms_ajax_text_plugin.urls')),
    )



urlpatterns += patterns('',
    url(r'^', include('cms.urls')),
)

