from django.utils.translation import ugettext_lazy as _
import sys

class AppSettings(object):
    __defaults = {
            'TEMPLATE_VARS': {},
            'FAVICON_URL': 'http://www.entrouvert.com/favicon.ico',
            'WCSINST_DEFAULT_VARIABLES':  {},
            'AUQUOTIDIENAPI_DEFAULTS': {
                'order': 1,
                'hash_algo': 'sha256',
                'signature_key': '12345',
                'verify_certificate': True,
                'allow_redirects': False,
            },
            'PROFILE_FORM_PLUGIN_FORM_CLASS': 'portail_citoyen.forms.ProfileFormPluginForm',
            'POSTAL_CODE_REGEXP': r'^[0-9]*$',
            'POSTAL_CODE_MESSAGE': _('Postal code must be five numbers'),
            'HOME_PHONE_REGEXP': r'^0[1234589][0-9]{8}$',
            'HOME_PHONE_MESSAGE': _('Phone number must start with 01, 02, 03, 04, 05, 08 or 07 and be ten digits long without spaces'),
            'MOBILE_PHONE_REGEXP': r'^0[67][0-9]{8}$',
            'MOBILE_PHONE_MESSAGE': _('Mobile phone number must start with 06 or 07 and be ten digits long without spaces'),

    }

    __prefix = 'PORTAIL_CITOYEN_'

    def __getattr__(self, name):
        from django.conf import settings
        if name not in self.__defaults:
            raise AttributeError
        return getattr(settings, self.__prefix + name, self.__defaults[name])

app_settings = AppSettings()
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
