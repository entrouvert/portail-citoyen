def callable_has_arg(method, arg):
    if hasattr(method, 'need_'+arg):
        return getattr(method, 'need_'+arg)
    if hasattr(method, 'im_func'):
        varnames = method.im_func.func_code.co_varnames
        argcount = method.im_func.func_code.co_argcount
        return arg in varnames[1:argcount]
    else:
        varnames = method.func_code.co_varnames
        argcount = method.func_code.co_argcount
        return arg in varnames[:argcount]
