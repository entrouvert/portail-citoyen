from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from models import A2ServiceListPlugin as A2ServiceListPluginModel

from authentic2.views import service_list

class A2ServiceListPlugin(CMSPluginBase):
    model = A2ServiceListPluginModel
    name = _('Authentic2 Service List Plugin')
    render_template = "a2_service_list_plugin/plugin.html"
    text_enabled = True

    def render(self, context, instance, placeholdre):
        context.update({
            'services': service_list(context['request']),
        })
        return context

plugin_pool.register_plugin(A2ServiceListPlugin)
