import sys

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


class AppSettings(object):
    '''Thanks django-allauth'''
    __SENTINEL = object()

    def __init__(self, prefix):
        self.prefix = prefix

    def _setting(self, name, dflt=__SENTINEL):
        from django.conf import settings
        v  = getattr(settings, self.prefix + name, dflt)
        if v is self.__SENTINEL:
            raise ImproperlyConfigured('Missing setting %r' % (self.prefix + name))
        return v

    @property
    def authorize_url(self):
        return self._setting('AUTHORIZE_URL')

    @property
    def token_url(self):
        return self._setting('TOKEN_URL')

    @property
    def api_url(self):
        return self._setting('API_URL')

    @property
    def client_id(self):
        return self._setting('CLIENT_ID')

    @property
    def client_secret(self):
        return self._setting('CLIENT_SECRET')

    @property
    def client_certificate(self):
        return self._setting('CLIENT_CERTIFICATE', None)

    @property
    def verify_certificate(self):
        return self._setting('VERIFY_CERTIFICATE', False)

    @property
    def client_credentials(self):
        return self._setting('CLIENT_CREDENTIALS', ())

    @property
    def more_url(self):
        return self._setting('MORE_URL', 'https://mon.service-public.fr/')


app_settings = AppSettings('MSP_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
