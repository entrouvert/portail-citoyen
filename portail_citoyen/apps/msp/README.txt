The MSP application provides authentication through MSP for Django application
and a gateway to the MSP REST API to share it with other properly authentified
applications.

Requirements
============

- Your base template must use django-sekizai and must contain a sekizai block
  named "css" and another named "js" respectively for stylesheet and
  javascript files.

- You must user a shared cache backend: through memcached, redis or the ORM.
  For example if you have a memcached installed just add the following
  fragment to your settings::

    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        }
    }

Installation
============

Add the application to your installed apps::

   INSTALLED_APPS += ( 'msp', )

Install the authentication backend::

   AUTHENTICATION_BACKENDS += ( 'msp.backends.MspBackend', )

Define needed settings, we show here the default values::

   MSP_AUTHORIZE_URL = 'https://mon.service-public.fr/apis/app/oauth/authorize'
   MSP_TOKEN_URL = 'https://mon.service-public.fr/apis/app/oauth/token'
   MSP_API_URL = 'https://mon.service-public.fr/apis/'
   MSP_CLIENT_ID = 'id assigned by DIMAP'
   MSP_CLIENT_SECRET = 'secret assigned by DIMAP'
   MSP_CLIENT_CERTIFICATE = ('/my-path/my-certificate.crt', '/my-path/my-certificate.key')
   MSP_VERIFY_CERTIFICATE = False

You must plug the application views in your urls.py file by adding this
content::

   url(r'^msp/', include('msp.urls')),

To link your account to MSP or unlink your account from MSP, add the following
content to your template::

   {% include 'msp/linking.html' %}

It will show a linking link when unauthenticated and when no msp account is
linked to the current account or an unlinking link when authenticated and a
to MSP exists.

To show a connection box include this content in your template::

   {% include 'msp/connecting.html' %}

To make the include file use a popup to talk to MSP add the popup parameter
like in the following content::

   {% include 'msp/connecting.html' with popup=1 %}

MSP Gateway
===========

The msp application also provides an OAuth2 gateway to MSP. To configure it your
just need to provider a list of client_id, client_secret pairs in your
settings, like that::

    MSP_CLIENT_CREDENTIALS = (('client_id1', 'client_secret1'),)

The following URL are provided::

- /authorize : like the authorize URL of MSP
- /access_token : like the access_token URL of MSP
- /documents/ : like the document list REST API endpoint of MSP
- /documents/<id>/ : like the document retrieval REST API endpoint of MSP
