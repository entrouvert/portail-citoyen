(function(undef) {
  function openpopup(path) {
        var windowName = 'ConnectWithOauth';
        var windowOptions = 'location=0,status=0,width=400,height=500';
        var oauthWindow = window.open(path, windowName, windowOptions);
        var oauthInterval = window.setInterval(function(){
            if (oauthWindow.closed) {
                window.clearInterval(oauthInterval);
            }
        }, 1000);
        window.open(path, windowName, windowOptions);
      return false;
  }
  var tags = document.getElementsByClassName('js-oauth-popup');
  for (var i = 0; i < tags.length; i++) {
    var tag = tags[i];
    tag.onclick = function (ev) {
      openpopup(this.href);
      return false;
    };
  }
})();
