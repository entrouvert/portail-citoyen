import requests
import json
import logging

logger = logging.getLogger(__name__)

from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from . import app_settings

class MspAccount(models.Model):
    user = models.OneToOneField(get_user_model(), verbose_name=_('user'))
    agc = models.CharField(max_length=64, verbose_name=_('access grant code'))
    token = models.TextField(verbose_name=_('access token'))

    def refresh_token(self):
        if not self.token:
            return True
        token = json.loads(self.token)
        data = {
                'grant_type': 'refresh_token',
                'refresh_token': token['refresh_token'],
                'client_id': app_settings.client_id,
                'client_secret': app_settings.client_secret,
        }
        response = requests.post(app_settings.token_url,
                data=data, verify=app_settings.verify_certificate,
                cert=app_settings.client_certificate)

        new_token = response.json()
        if 'error' in new_token:
            if new_token['error'] == 'invalid_grant':
                logger.warning('obsolete token %r, deleting MspAccount %r', self.token,
                        self.agc)
                self.delete()
                return False
            return True
        else:
            self.token = json.dumps(new_token)
            self.save()
            return True
