from django.conf.urls import patterns, url

urlpatterns = patterns('portail_citoyen.apps.msp.views',
    url(r'^login/$', 'login', name='msp-login'),
    url(r'^link/$', 'link', name='msp-link'),
    url(r'^login-or-link/$', 'login_or_link', name='msp-login-or-link'),
    url(r'^link-management/$', 'link_management', name='msp-link-management'),
    url(r'^link-management/unlink/confirm/$', 'confirm_unlink', name='msp-confirm-unlink'),
    url(r'^link-management/unlink/done/$', 'unlink_done', name='msp-unlink-done'),
    url(r'^link-management/unlink/$', 'unlink', name='msp-unlink'),
    url(r'^authorize/$', 'authorize', name='msp-authorize'),
    url(r'^access_token/$', 'access_token', name='msp-access-token'),
    url(r'^documents/$', 'documents', name='msp-documents'),
    url(r'^documents/(?P<doc_id>[^/]*)/$', 'document', name='msp-document'),
    url(r'^more/$', 'more_redirect', name='msp-more-redirect'),
)
