# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
            ('cms', '0039_auto__del_field_page_moderator_state'),
    )

    def forwards(self, orm):
        # Adding model 'DataSource'
        db.create_table(u'data_source_plugin_datasource', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('mime_type', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=1024)),
        ))
        db.send_create_signal(u'data_source_plugin', ['DataSource'])

        # Adding model 'PluginDataSource'
        db.create_table(u'data_source_plugin_plugindatasource', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='plugins', to=orm['data_source_plugin.DataSource'])),
            ('plugin', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sources', to=orm['data_source_plugin.DataSourcePlugin'])),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'data_source_plugin', ['PluginDataSource'])

        # Adding model 'RawInlineTemplatePlugin'
        db.create_table(u'cmsplugin_rawinlinetemplateplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('template_source', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal(u'data_source_plugin', ['RawInlineTemplatePlugin'])

        # Adding model 'DataSourcePlugin'
        db.create_table(u'cmsplugin_datasourceplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('template_source', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('refresh', self.gf('django.db.models.fields.IntegerField')(default=60)),
        ))
        db.send_create_signal(u'data_source_plugin', ['DataSourcePlugin'])


    def backwards(self, orm):
        # Deleting model 'DataSource'
        db.delete_table(u'data_source_plugin_datasource')

        # Deleting model 'PluginDataSource'
        db.delete_table(u'data_source_plugin_plugindatasource')

        # Deleting model 'RawInlineTemplatePlugin'
        db.delete_table(u'cmsplugin_rawinlinetemplateplugin')

        # Deleting model 'DataSourcePlugin'
        db.delete_table(u'cmsplugin_datasourceplugin')


    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 4, 10, 0, 0)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'data_source_plugin.datasource': {
            'Meta': {'ordering': "('name',)", 'object_name': 'DataSource'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mime_type': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '1024'})
        },
        u'data_source_plugin.datasourceplugin': {
            'Meta': {'object_name': 'DataSourcePlugin', 'db_table': "u'cmsplugin_datasourceplugin'"},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'refresh': ('django.db.models.fields.IntegerField', [], {'default': '60'}),
            'template_source': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'})
        },
        u'data_source_plugin.plugindatasource': {
            'Meta': {'ordering': "('order', 'id')", 'object_name': 'PluginDataSource'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'plugin': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sources'", 'to': u"orm['data_source_plugin.DataSourcePlugin']"}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'plugins'", 'to': u"orm['data_source_plugin.DataSource']"})
        },
        u'data_source_plugin.rawinlinetemplateplugin': {
            'Meta': {'object_name': 'RawInlineTemplatePlugin', 'db_table': "u'cmsplugin_rawinlinetemplateplugin'"},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'template_source': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'})
        }
    }

    complete_apps = ['data_source_plugin']
