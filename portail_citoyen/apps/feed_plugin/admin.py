from django.contrib import admin

from . import models

class FeedAdmin(admin.ModelAdmin):
    list_display = [ 'name', 'url', 'color_hex', 'css_classes' ]

admin.site.register(models.Feed, FeedAdmin)
