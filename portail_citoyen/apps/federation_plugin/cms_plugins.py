from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from authentic2.saml.models import LibertyFederation

import models


class FederationPlugin(CMSPluginBase):
    model = models.FederationPlugin
    name = _('federation management plugin')
    render_template = 'federation_plugin/plugin.html'
    text_enabled = True

    def render(self, context, instance, placeholder):
        request = context['request']
        context['submit'] = submit = 'submit-{class_name}-{instance_id}'.format(
                class_name=self.__class__.__name__.lower(),
                instance_id=instance.id)
        providers = instance.get_providers()
        federations = LibertyFederation.objects.filter(user=request.user)
        if providers:
            federations = federations.filter(sp__liberty_provider__in=providers)
        if request.method == 'POST' and submit in request.POST:
            federation_id = request.POST.get('federation_id', '0')
            federations.filter(id=federation_id).update(user=None)
        context['federations'] = []
        for federation in federations:
            context['federations'].append({
                'name': federation.sp.liberty_provider.name,
                'slug': federation.sp.liberty_provider.slug,
                'hidden_values': [
                    ('federation_id', federation.id),
                ],
            })
        context['instance'] = instance
        return context


plugin_pool.register_plugin(FederationPlugin)
