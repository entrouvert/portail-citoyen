import logging


from django.db import models
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError

from authentic2.saml.models import LibertyProvider

from cms.models import CMSPlugin


logger = logging.getLogger(__name__)


class FederationPlugin(CMSPlugin):
    '''Django CMS plugin to manipulate Authentic2 federations'''

    provider_restriction = models.TextField(verbose_name=_('provider '
        'restrictions'), blank=True, help_text=_('provides provider slug '
            'separated by commas, appended or prepended spaces will be '
            'removed'))

    def get_slugs(self):
        return filter(None, map(unicode.strip, self.provider_restriction.split(',')))

    def get_providers(self):
        return LibertyProvider.objects.filter(slug__in=self.get_slugs())

    def clean(self):
        self.provider_restriction = u','.join(self.provider_restriction.split(','))
        for slug in self.get_slugs():
            if not LibertyProvider.objects.filter(slug=slug).exists():
                raise ValidationError(_('no provider with slug %r') % slug)
