import sys
import locale
import logging
from optparse import make_option

from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import FieldDoesNotExist

from django.core import serializers

from allauth.socialaccount.models import SocialApp, SocialAccount
from portail_citoyen.apps.feed_plugin.models import Feed


class Command(BaseCommand):
    args = ''
    help = '''Migrate portail citoyen to authentic'''
    option_list = BaseCommand.option_list + (
            make_option('--group', action="append"),
    )

    def get_objects(self, group=None, **options):
        # butt ugly work around the fact that the classical user model is not
        # initialized, so we need to simulate many2many fields and also to
        # simulate "through" models of those sames fields in order to make the
        # JSON serializer happy
        class X(object):
            def __init__(self, **kwargs):
                self.__dict__.update(kwargs)
        class MockManager(object):
            def __init__(self, seq):
                self.seq = seq

            def iterator(self):
                return self.seq
        SocialApp._meta.many_to_many[0].rel.through = X(_meta=X(auto_created=True))
        class MockManagerDescriptorEmpty(object):
            def __get__(self, instance, instance_type=None):
                return MockManager([])
        SocialApp.sites = MockManagerDescriptorEmpty()
        social_app = SocialApp(
                id=1,
                provider='authentic2',
                name='Compte citoyen',
                client_id='1',
                secret='1234')
        social_app.__dict__['sites'] = MockManager([])
        yield social_app

        for m2m_field in User._meta.many_to_many:
            m2m_field.rel.through = X(_meta=X(auto_created=True))
        class MockManagerDescriptorGroups(object):
            def __get__(self, instance, instance_type=None):
                return instance.__dict__['groups']
        User.groups = MockManagerDescriptorGroups()
        User.user_permissions = MockManagerDescriptorEmpty()
        # fix user fk on SocialAccount
        BASE_FIELDS = ('username', 'email', 'first_name', 'last_name',
                'last_login', 'password', 'date_joined', 'is_active', 'is_admin', 'is_staff')
        user_model = get_user_model()
        for feed in Feed.objects.all():
            yield feed
        for user in user_model.objects.prefetch_related('feedpreference_set'):
            new_user = User()
            new_user.__dict__['groups'] = user.groups.filter(name__in=group)
            for field in BASE_FIELDS:
                try:
                    user_model._meta.get_field(field)
                except FieldDoesNotExist:
                    continue
                setattr(new_user, field, getattr(user, field))
            yield new_user
            social_account = SocialAccount(user=user, provider='authentic2')
            if hasattr(user_model, 'USERNAME_FIELD'):
                social_account.uid = getattr(user, user_model.USERNAME_FIELD)
            else:
                social_account.uid = user.username
            if hasattr(user, 'last_login'):
                social_account.last_login = user.last_login
            if hasattr(user,'date_joined'):
                social_account.date_joined = user.date_joined
            yield social_account
            for feed_preference in user.feedpreference_set.all():
                yield feed_preference

    @transaction.commit_on_success
    def handle(self, *args, **options):
        self.logger = logging.getLogger()
        locale.setlocale(locale.LC_ALL, '')
        handler = logging.StreamHandler(stream=sys.stderr)
        self.logger.addHandler(handler)
        if options['verbosity'] > 2:
            handler.setLevel(level=logging.DEBUG)
        json_serializer = serializers.get_serializer('json')()
        json_serializer.serialize(self.get_objects(**options), indent=4,
                stream=sys.stdout, use_natural_keys=True)
