import json
from optparse import make_option
import re

from django.core.management.base import BaseCommand
from django.db.models.loading import get_model
from django.contrib.sites.models import Site

from cms.models import Page, CMSPlugin, Title, Placeholder

# Title -> page
# Page -> parent
# Placehodler page.placeholders.add()
# CMSPlugin placeholder -> , parent ->


class Command(BaseCommand):
    help = 'Imports CMS page and its content'
    option_list = BaseCommand.option_list + (
        make_option('--dump', action='store', default='dump.json'),
    )

    def _get_model(self, model_label):
        return get_model(*model_label.split('.'))

    def load_object(self, obj):
        model = obj['model'].split('.')[1]
        try:
            getattr(self, 'load_%s' % model)(obj['fields'])
        except AttributeError:
            self.load_plugin(obj)

    def load_plugin(self, obj):
        model = self._get_model(obj['model'])
        parent = obj['fields'].pop('parent')
        placeholder = obj['fields'].pop('placeholder')
        placeholder = Placeholder.objects.get(slot=placeholder[1],
                                              page__title_set__path__in=(placeholder[0],),
                                              page__publisher_draft__isnull=True)
        obj['fields']['placeholder'] = placeholder
        if parent:
            parent = CMSPlugin.objects.get(position=parent[3],
                                           plugin_type=parent[2],
                                           placeholder=placeholder)
            instance, klass = parent.get_plugin_instance()
            obj['fields']['parent'] = parent
        new_obj = model(**obj['fields'])
        new_obj.save(no_signals=True)

        # update child's id in parent's body
        if parent and hasattr(instance, 'body'):
            instance.body = re.sub('plugin_obj_\d+',
                                   'plugin_obj_%s' % new_obj.id, instance.body)
            instance.save()

    def get_page_parent(self, page_path):

        if page_path:
            return Page.objects.get(title_set__path__in=(page_path,), publisher_is_draft=True)
        return Page.objects.get_home().publisher_draft

    def get_title_parent(self, page_path):

        try:
            parent_title, child_title = page_path[0].rsplit('/', 1)
        except ValueError:
            parent_title = None
        parent = self.get_page_parent(parent_title)
        return parent.children.get(title_set__isnull=True,
                                   publisher_is_draft=True,
                                   publisher_draft__isnull=True)

    def load_page(self, obj):
        obj.pop('publisher_public')

        # because a page needs a site for unknown reason!
        obj['site'] = Site.objects.get_current()
        if obj['parent']:
            parent = self.get_page_parent(obj['parent'][0])
        else:
            parent = self.get_page_parent(obj['parent'])
        obj['parent'] = parent
        page = Page(**obj)
        page.save(no_signals=True)

    def load_placeholder(self, obj):
        page = obj.pop('page')
        page = Page.objects.get(title_set__path__in=page, publisher_draft__isnull=True)
        placeholder = Placeholder(**obj)
        placeholder.save()
        page.placeholders.add(placeholder)

    def load_title(self, obj):
        obj['page'] = self.get_title_parent(obj['page'])
        title = Title(**obj)
        title.save()

    def handle(self, *args, **options):
        with open(options.get('dump')) as dump:
            data = json.load(dump)
            for fixture in data:
                self.load_object(fixture)
