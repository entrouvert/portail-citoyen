import os

# Multitenant settings
TENANT_MODEL = 'multitenant.Tenant'

MIDDLEWARE_CLASSES = (
        'tenant_schemas.middleware.TenantMiddleware',
        'entrouvert.djommon.multitenant.middleware.EOTenantMiddleware',
)

MIDDLEWARE_CLASSES = (
    'tenant_schemas.middleware.TenantMiddleware',
    'entrouvert.djommon.multitenant.middleware.EOTenantMiddleware',
    'entrouvert.djommon.middleware.VersionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.http.ConditionalGetMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.transaction.TransactionMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'authentic2.idp.middleware.DebugMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)


TEMPLATE_LOADERS = (
    'entrouvert.djommon.multitenant.template_loader.FilesystemLoader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

MULTITENANT_TEMPLATE_DIRS = ('/var/lib/my_project/templates/',)

TENANT_APPS = (
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'registration',
    'south',
    'mptt',
    'cms',
    'authentic2',
    'authentic2.nonce',
    'authentic2.saml',
    'authentic2.idp',
    'authentic2.idp.saml',
    'authentic2.auth2_auth',
    'authentic2.attribute_aggregator',
    'authentic2.disco_service',
    'menus',
    'cms.plugins.file',
    'cms.plugins.googlemap',
    'cms.plugins.link',
    'cms.plugins.picture',
    'cms.plugins.snippet',
    'cms.plugins.teaser',
    'cms.plugins.video',
    'cmsplugin_text_wrapper',
    'cms_ajax_text_plugin',
    'portail_citoyen.apps.feed_plugin',
    'portail_citoyen.apps.data_source_plugin',
    'portail_citoyen.apps.a2_service_list_plugin',
    'portail_citoyen.apps.federation_plugin',
    'portail_citoyen.apps.auquotidien_plugin',
    'sekizai',
    'portail_citoyen',
)

SHARED_APPS = (
        'tenant_schemas',
        'entrouvert.djommon.multitenant',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'django.contrib.sites',
)

INSTALLED_APPS = TENANT_APPS + ('tenant_schemas', 'entrouvert.djommon.multitenant')

DATABASES = {
    'default': {
        'ENGINE': 'tenant_schemas.postgresql_backend',
        'NAME': os.environ.get('MULTITENANT_DATABASE', 'portail-citoyen'),
    },
}

SOUTH_DATABASE_ADAPTERS = {
    'default': 'south.db.postgresql_psycopg2',
}
