from django.utils.translation import ugettext_lazy as _, ugettext as _2
from django.core.exceptions import ImproperlyConfigured
from django.forms import ModelForm, Form


from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool

from cms.models.pluginmodel import CMSPlugin

from . import utils, app_settings


class FormPluginBase(CMSPluginBase):
    form_class = None
    no_cancel_button = True

    def get_form_class(self, request, context, instance, placeholder):
        return self.form_class

    def render(self, context, instance, placeholder):
        request = context['request']
        class_name = self.__class__.__name__.lower()
        context['submit_name'] = submit = 'submit-{class_name}-{instance_id}'.format(
                class_name=class_name,
                instance_id=instance.id)
        context['submit_value'] = _2('Modify')
        context['instance'] = instance
        context['no_cancel_button'] = self.no_cancel_button
        kwargs = {'prefix': class_name}
        form_class = self.get_form_class(request, context, instance,
                placeholder)
        if issubclass(form_class, ModelForm):
            if not hasattr(self, 'get_form_instance'):
                raise ImproperlyConfigured('Your plugin class is missing a get_form_instance method but use a ModelForm')
            kwargs['instance'] = context['object'] = \
                    self.get_form_instance(request, context, instance, placeholder)
        if utils.callable_has_arg(form_class.__init__, 'plugin_instance'):
            kwargs['plugin_instance'] = instance
        if utils.callable_has_arg(form_class.__init__, 'request'):
            kwargs['request'] = request
        if request.method == 'POST' and submit in request.POST:
            form  = form_class(data=request.POST, **kwargs)
            if form.is_valid():
                form.save()
        else:
            form  = form_class(**kwargs)
        context['form'] = form
        return context


class ProfileFormPlugin(FormPluginBase):
    model = CMSPlugin
    name = _('account form plugin')
    render_template = 'portail_citoyen/profile_form_plugin.html'
    text_enabled = True

    def get_form_class(self, request, context, instance, placeholder):
        if request.user.is_anonymous():
            return Form
        try:
            import importlib
        except ImportError:
            from django.utils import importlib
        form_class_path = app_settings.PROFILE_FORM_PLUGIN_FORM_CLASS
        module_path, class_name = form_class_path.rsplit('.', 1)
        module = importlib.import_module(module_path)
        return getattr(module, class_name)

    def get_form_instance(self, request, context, instance, placeholder):
        return request.user
plugin_pool.register_plugin(ProfileFormPlugin)

class AccountManagementPlugin(CMSPluginBase):
    model = CMSPlugin
    name = _('account management plugin')
    render_template = 'portail_citoyen/account_management_plugin.html'
    text_enabled = True

plugin_pool.register_plugin(AccountManagementPlugin)
