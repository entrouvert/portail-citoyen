from django import template

register = template.Library()

@register.assignment_tag(takes_context=True)
def user_in_group(context, group):
    user = context['user']
    return user.groups.filter(name=group).exists()


@register.assignment_tag(takes_context=True)
def user_in_group_prefix(context, group):
    user = context['user']
    return user.groups.filter(name__startswith=group).exists()
