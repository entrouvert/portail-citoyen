#! /usr/bin/env python

''' Setup script for Polynum
'''

import glob
import re
import sys
import os

from setuptools import setup, find_packages
from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from distutils.command.sdist import sdist
from distutils.cmd import Command

class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management.commands.compilemessages import \
                compile_messages
            for path in ['portail_citoyen'] + glob.glob('portail_citoyen/apps/*'):
                if path.endswith('.py'):
                    continue
                if not os.path.isdir(os.path.join(path, 'locale')):
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                compile_messages(sys.stderr)
                os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print
            print

class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands

class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)

class eo_sdist(sdist):

    def run(self):
        print "creating VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        print "removing VERSION file"
        if os.path.exists('VERSION'):
            os.remove('VERSION')

def get_version():

    version = None
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    for d in glob.glob('*'):
        if not os.path.isdir(d):
            continue
        module_file = os.path.join(d, '__init__.py')
        if not os.path.exists(module_file):
            continue
        for v in re.findall("""__version__ *= *['"](.*)['"]""",
                open(module_file).read()):
            assert version is None
            version = v
        if version:
            break
    assert version is not None
    if os.path.exists('.git'):
        import subprocess
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        assert p.returncode == 0, 'git returned non-zero'
        new_version = result.split()[0][1:]
        assert new_version.split('-')[0] == version, '__version__ must match the last git annotated tag'
        version = new_version.replace('-', '.')
    return version


setup(name="portail_citoyen",
      version=get_version(),
      license="AGPLv3 or later",
      description="Portail citoyen",
      url="http://dev.entrouvert.org/projects/portail-citoyen/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Benjamin Dauvergne",
      maintainer_email="info@entrouvert.com",
      include_package_data=True,
      packages=find_packages(),
      scripts=('portail-citoyen',),
      setup_requires=[
            'django>=1.5.1,<1.6',
      ],
      install_requires=[
            'requests>=2.1',
            'feedparser',
            'django>=1.5.1,<1.6',
            'authentic2>2.0.2',
            'django-cms>=2.5',
            'python-entrouvert',
            'south>=0.8.4',
            'djangocms-text-ckeditor',
            'Pillow',
      ],
      dependency_links = [
          'git+git://repos.entrouvert.org/authentic.git/#egg=authentic2-9999.0',
          'git+git://repos.entrouvert.org/python-entrouvert.git/#egg=python-entrouvert-9999.0',
          'git+git://repos.entrouvert.org/django-cms.git/@develop#egg=django-cms-2.4.1.9999',
      ],
      cmdclass={'build': build, 'install_lib': install_lib,
          'compile_translations': compile_translations,
          'sdist': eo_sdist},
)
