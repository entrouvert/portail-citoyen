#!/bin/bash -e
pip install --upgrade setuptools
pip install --upgrade pip
pip install --upgrade pylint
pip install --upgrade pyOpenSSL ndg-httpsclient requests pyasn1
sed -i 's/^MAX = 64/MAX = 200/' $VIRTUAL_ENV/lib/python*/site-packages/ndg/httpsclient/subj_alt_name.py

pip install --upgrade -r requirements.txt

./portail-citoyen syncdb --migrate --noinput --no-initial-data
./portail-citoyen loaddata initial_data
./portail-citoyen validate
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc portail_citoyen/ | tee pylint.out) || /bin/true
